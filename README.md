# TTM00-FW-PIC32-Training-V1

## Project Brief

In this project the first meeting with the PIC32MX470 microcontroller for the FW module of the Titoma catedra will be held.

## Hardware Description

* PIC32MX470F512H: https://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX330350370430450470_Datasheet_DS60001185H.pdf
* Curiosity board user guide: https://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf

## Serial commands

## Prerequisites

1. SDK Version:
    * IDE version: MPLAB X IDE v5.50
    * Compiler version: xc32 v1.42
    * Project configuration
        * Categories: Microchip Embedded
        * Projects: Standalone Project
        * Family: All families
        * Device: PIC32MX470F512H
        * Hardware tools: Microchip starter kit
        * Compiler: XC32 (v1.42)
        * Encoding: ISO-8859-1

## Versioning

1. Current version of the FW
    * V1.0.20210714

## Authors

    1. Project staff: Jose Alberto Granada Rodas
    2. Maintainer contact email: josegranadajag@gmail.com
